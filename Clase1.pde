// comentarios
/* asi tambien */

float ejemplo = 5;
float angulo;

// ciclo de vida!

// corre una vez al inicio
void settings() {
  size(640, 480, P3D);
  angulo = 0;
}

// corre una vez al inicio (después)
void setup() {
  println("setup");
  resetMatrix();
  printMatrix();
}

// corre una vez por cuadro
// cuadro - proceso de redibujado, sucede muchas veces por segundo
// 30 fps + - aplicación en tiempo real
void draw() {
  // rgb - red, green, blue
  // 0 - 255
  background(255, 255, 255);

  // color de lineas
  //stroke(0, 255, 255);

  // color de relleno
  // fill(255, 0, 255);

  noFill();

  // SITUACIÓN: cada vez que hacemos una transformación modificamos la matriz que define el estado del mundo
  // TODOS los vertices son afectados por esta matriz
  
  // podemos guardar estados específicos de la matriz en una estructura
  // podemos sacar de vuelta esos estados

  // cada transformación es una matriz de 4x4 que se multiplica con la matriz del estado actual
  // las transformaciones se aplican en orden inverso
  translate(-120, -40, -415);
  
  // push matrix guarda el estado actual de la matriz en el stack
  // no altera / afecta la matriz en su estado actual
  pushMatrix();
  
  rotateY(angulo);
  translate(-150, -150, -50);
  //printMatrix();

  //frente
  beginShape();
  vertex(100, 100, 0);
  vertex(200, 100, 0);
  vertex(200, 200, 0);
  vertex(100, 200, 0);
  endShape(CLOSE);

  // derecho
  beginShape();
  vertex(200, 100, 0);
  vertex(200, 100, 100);
  vertex(200, 200, 100);
  vertex(200, 200, 0);
  endShape(CLOSE);

  // atras
  beginShape();
  vertex(100, 100, 100);
  vertex(200, 100, 100);
  vertex(200, 200, 100);
  vertex(100, 200, 100);
  endShape(CLOSE);

  // izquierdo
  beginShape();
  vertex(100, 100, 0);
  vertex(100, 100, 100);
  vertex(100, 200, 100);
  vertex(100, 200, 0);
  endShape(CLOSE);

  // al hacer pop recuperarmos el último estado guardado de la matriz que estaba en el stack
  // cargar la matriz de vuelta como estado 
  popMatrix();

  pushMatrix();
  translate(200, 0, 0);
  scale(2);
  rotateY(-angulo);
  
  box(50);
  
  popMatrix();
  
  pushMatrix();
  translate(100, 150, 0);
  scale(2);
  rotateX(angulo);

  box(50);
  popMatrix();
  
  translate(100, -150, 0);
  scale(1, 2, 1);
  rotateX(-angulo);
  
  box(50);

  angulo += 0.05f;
}

void mousePressed() {
  //rect(mouseX, mouseY, 100, 100);
}

void keyPressed() {
  println(key);
  println(keyCode);
}
